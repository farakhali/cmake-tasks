# cmake_tasks

This repo include some basic prctice questions and Tasks related to CMake. First you have to clone the Repository using commands...

- `git clone (URL/HTTPS)`
- `cd cmake_tasks`

## How to Run `task_1`

First practice question is simple Hello World program which is executed using the cmake. To view the output of this tasks follow these steps.

---

- cd task_1
- mkdir build
- cd build
- cmake ..
- make

---

The main executable is named `./hello`.

## How to Run `task_2`

This is simple calculator (add, subtract, divide and mltiple) any given numbers. A Static libraray of calculator is created and linked with main application. To view the output of this tasks follow these steps.

### Build in Default mode

---

- cd task_2
- mkdir build
- cd build
- cmake ..
- make

---

### Set Mode using CMakeLists.txt file

- To set build mode in CMakeLists.txt add the following line to your cmakefile `set(CMAKE_BUILD_TYPE Debug)`

### Set Mode using command line

| Compile in Release mode                                | Compile in Debug mode                                |
| ------------------------------------------------------ | ---------------------------------------------------- |
| `cd build`                                             | `cd build`                                           |
| `cmake -DCMAKE_BUILD_TYPE=Release .. (path/to/source)` | `cmake -DCMAKE_BUILD_TYPE=Debug .. (path/to/source)` |
| `make`                                                 | `make`                                               |

The main executable is named `./task-2`.

## How to Run `task-2_shared`

Both the _*task_2*_ and _*task-2_shared*_ are almost same. In `task_2` Static libraries are created and linked with main application, in `task-2_shared` Shared/Dynamic libraries are created and linked to main appliction. To build the `task-2_shared` follow the same steps as above in _task_2_.

## How to Run `task-3_third-party-library`

This is a simple task which include the usue of any third party library to link it in your main and use dome of its function to get familier with that concept in cmake. I use the Submodule procedure to link the third party library and I linked the `nlohmann_json` library and use it in main application. In main application I use the `dump()` function of that library and dump dome data into a txt file. To build the code follow these steps..

---

- `cd task-3_third-party-library`
- `git submodule update --init`
- `mkdir build`
- `cd build`
- `cmake ..`
- `make`
- `./main`

---

## How to Run `task_config`

In this task I use config file to give user option to claculate square root of any number by C-math library or by static square root library created by me. Steps to follow...

- `c task_config`
- `mkdir build`
- `cd build`
- `cmake -DUSE_MYMATH=(ON or OFF) -DCMAKE_BUILD_TYPE=(Debug or Release) ..`
- `make`
- `./Tutorial number`

The `configure.h.in` also include install procedure for MathFunctions, we need to install libraries and header files, for applications, we need to install executable files and configuration header files. To install use the following command.

- `cd build`
- `sudo cmake --install .`

Now to calculate square root use `Tutorial 4(number)`
