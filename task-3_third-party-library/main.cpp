#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

int main()
{
    // create JSON values
    json j_object = {{"one", 1}, {"two", 2}};
    json j_dict ={ 
    "emp1", 
        {
        {"name", "Lisa"},
        {"designation", "programmer"},
        {"age", 34},
        {"salary", 54000}
        },
    "emp2", 
        {
        {"name", "Elis"},
        {"designation", "Trainee"},
        {"age", 24},
        {"salary", 40000}
        },
    };

   // call dump()
    std::cout << "objects : " << j_object.dump(-4) << "\n";
    std::cout << "arrays : " <<  j_dict.dump('\t') << "\n";
    
    std::ofstream myfile;
    myfile.open ("/home/emumba/Documents/cmake/cmake-tasks/example.txt");
    std::cout << "Dumping Values...\n";
    myfile << "JSON Values.\n";
    myfile <<"objects : \n" << j_object.dump('\t') 
        << "\nDict : \n"<< j_dict.dump('\t') << "\n";
    myfile.close();
}
