cmake_minimum_required(VERSION 3.16.3)

set( CMAKE_CXX_FLAGS "-Os -w -Wall" )

add_library(calculator SHARED calc.cpp)

