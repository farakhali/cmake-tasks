/* filename: driver.c */
#include <iostream>
#include <calc.h>
#include <mymath.h>

using namespace std;

int main(void)
{
    float number_1, number_2;
    int option;
    cout<<"This is simple calculaator program in C++\n"<<endl;
    cout<<"Enter First number"<<endl;    
    cin>>number_1;
    cout<<"Enter second number"<<endl;    
    cin>>number_2;

    cout<< "Choices \t 1 : ADD \t 2 : Subtract \t 3 : Divide \t 4 : Multiple \t" <<endl;

    cout<<"Enter your choice"<<endl;    
    cin>>option;

    calculator(number_1, number_2, option);

	cout << "\nCalling function from second library \n "<<endl;

	math();
}